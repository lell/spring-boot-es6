package com.sillyteddy.dashboard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Kai Ching on 21/4/17.
 */

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String helloWorld() {
        return "hello";
    }

}
