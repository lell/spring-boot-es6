var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: './src/main/web/main.js',
	output: {
		filename: './target/classes/static/bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ["es2015", "react"]
				}
			},
			{test: /\.scss$/, loader: ExtractTextPlugin.extract('css-loader!sass-loader')}
		]
	},
	plugins: [
		new ExtractTextPlugin({
			filename : 'target/classes/static/css/style.css',
			allChunks: true
		})
	]
};